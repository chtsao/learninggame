---
title: About
date: 2019-09-15 13:56:14
---
![](https://images.gr-assets.com/authors/1401861378p5/1399243.jpg)
>只有當人完全成為人的時候，他才遊戲；只有當人遊戲的時候，他才是完整的人。
[美育書簡](https://www.hk01.com/%E5%93%B2%E5%AD%B8/53447/%E5%B8%AD%E5%8B%92-%E5%8F%AA%E6%9C%89%E5%AE%8C%E5%85%A8%E7%9A%84%E4%BA%BA%E6%89%8D%E6%9C%83%E9%81%8A%E6%88%B2-11-10)
-- <cite>席勒（Friedrich Schiller）</cite>
