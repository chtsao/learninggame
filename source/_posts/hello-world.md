---
title: Game and Learn
date: 2019-09-15 14:33:19
tags:
---
![](https://petplaceimages-embracepetinsura1.netdna-ssl.com/wp-content/uploads/2017/09/playing-with-your-cat-large.jpg?x37310)

天生內建的稟賦再加上後天的學習，我們得以成為一個在社會可以獨立生存的個體。雖然沒有詳細的科學分析，但似乎多數動物的學習都相當短而近似於遊戲，唯有人的學習時間非常長而且也比較不像遊戲。

